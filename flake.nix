{
  description = "An absolute beginner course to learn the basics of programming, using the Rust language";

  outputs = { self, nixpkgs }: {

    devShells.x86_64-linux.default = with nixpkgs.legacyPackages.x86_64-linux; mkShell {
      nativeBuildInputs = [
        rustc
        cargo
      ];
      buildInputs = [
        rust-analyzer
        rustfmt
        nixpkgs-fmt
        git
      ];
    };

    packages.x86_64-linux.hello = nixpkgs.legacyPackages.x86_64-linux.hello;

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;

  };
}
