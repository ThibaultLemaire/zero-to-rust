# Functions

En programmation, une fonction c'est conceptuellement une boîte, qui prend des choses en entrée, et les transforme pour cracher d'autres choses en sortie.

![Example function](./example-function.excalidraw.svg)

Un programme est un assemblage de fonctions qui appellent d'autres fonctions.

Le terme "fonction" est emprunté au mathématiques car c'est conceptuellement très proche.

Il y aurait beaucoup à dire sur les fonctions, mais concentrons-nous pour l'instant sur le vocabulaire programmatique. Une fonction a:
 - des __arguments__.
   Ce sont ses __entrées__ (__input__).
   Les valeurs passées à ces arguments lors d'un appel sont appelées __paramètres__ (__parameters__).
   Argument et paramètre sont souvent confondus, la différence n'est souvent pas essentielle.
 - une __sortie__ (__output__), aussi appelée "__retour__" (__return__).
   Il n'y en a toujours qu'une, mais il peut s'agir d'une structure de données (conteneur) contenant plusieurs valeurs, ce qui permet en pratique d'avoir plusieurs sorties.
   (Comme dans la fonction `decode_streams` dans l'illustration ci-dessus.)
 - un __nom__.
   (Oui, c'est évident. Non, il n'y a pas de piège.)
 - une __signature__.
   C'est la combinaison de son nom, ses arguments, et sa sortie.
   Ça permet d'identifier une fonction de manière unique, d'où le terme de signature.  
   (E.g. il n'y a qu'une fonction s'appelant `get_codec`, prenant en entrée un flux audio/video, et retournant en sortie le nom du codec utilisé)

# Fonctions "Impures"

Non, il ne s'agit pas de ce genre d'impureté [~~☣️✝️😈🔥~~](https://scp-wiki.wikidot.com/scp-093-recovered-materials)

On dit d'une fonction qu'elle est "pure" si elle n'a pas de "side effect", que l'on peut traduire par "effet de bord", mais également par "effet secondaire", comme sur les médicaments.
Ça signifie que sa signature décrit parfaitement ses effets et qu'elle ne fait rien en-dehors de ça.

En effet, certaines fonctions sont capables de sortir de leur boîte pour aller modifier des choses qui ne sont pas dans leur sortie.
Ce sont elles qui sont dites "impures", mais ce n'est pas forcément une mauvaise chose, comme on va le chapitre suivant:

---

## => ["Hello, world!"](./hello-world.md)
