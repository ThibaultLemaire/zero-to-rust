# C'est quoi le "code source" ?

Le code source c'est comme un dessin industriel, et/ou une recette de cuisine.

![engineering drawing](./what-is-programming-engineering-drawing.webp)
![cooking recipe](./what-is-programming-cooking-recipe.webp)

Comme des plans de montage, ça décrit une machine (un programme) qui, une fois construite (compilé) va réaliser les tâches souhaitées lorsqu'elle est lancée.

Comme une recette, ça prend la forme d'une suite d'instructions plus ou moins détaillées (plus ou moins "haut-niveau" (d'abstraction)), indiquant à la machine ce qu'elle doit faire.

Ça peut décrire, par exemple, un programme qui extrait le son d'un fichier vidéo et le sauvegarde dans un autre fichier (c.f. [ffmpeg](https://ffmpeg.org/)).

Ou quelque-chose de beaucoup plus complexe qui dessine des images dans une fenêtre, simulant un environnement 3D, et réagissant au frappes clavier, aux mouvements de la souris, et aux paquets réseau. (Un jeu vidéo)

Il existe une multitude de langages de programmation servant à (d)écrire des programmes.
Ils répondent tous à divers besoins/préférences de leurs auteur·ice·s et utilisateur·ice·s.

Pour avoir un avant-goût, tu peux aller lire, sur Wikipedia, [les exemples](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program#Examples) de code source d'un programme affichant ["Hello, world!"](./hello-world.md) dans un terminal, traduit en divers langages de programmation.

## Attribution

- [DIN 69893 hsk 63a drawing.png](https://commons.wikimedia.org/wiki/File:DIN_69893_hsk_63a_drawing.png) CC-BY-SA-2.5 Sven Gleich
- [Mom's Cold Beet Soup recipe](https://www.flickr.com/photos/cizauskas/3800595877/) CC-BY-NC-SA-2.0 Thomas Cizauskas

---

## => [Functions](./functions.md)
