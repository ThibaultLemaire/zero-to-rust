# Ton premier programme

A.k.a "Hello, world!"

Commençons ce chapitre par un tout petit peu de pratique:
Dans ton [bac à sable](./setup-dev-env.md)

```sh
cd ~/dev/zero-to-rust/playground
```

tu vas créer un nouveau projet d'executable (aussi appelé binaire) Rust

```sh
cargo init
```

Et le lancer

```sh
cargo run
```

La dernière ligne devrait lire "Hello, world!".

"Hello, world!", littéralement "Salut, monde!", c'est un peu le premier cri de ce programme nouveau-né.
Comme s'il venait au monde, et se sentait obligé de le saluer par politesse.
Alors, tel le Dr Frankenstein tu peux [le célébrer](https://youtu.be/1qNeGSJaQ9Q?t=152):

__Félicitations, tu viens d'écrire ton premier programme en Rust!__ 🎉  
(Ou plutôt, il a été auto-généré, mais on va en lire le code, donc ça compte!)

Ouvre ton éditeur de code, on va regarder à quoi ça ressemble!

```sh
codium .
```

> ⚠️ Syntaxe: le point (`.`) est important dans cette commande, il indique à l'éditeur qu'il doit ouvrir le répertoire courant (le dossier dans lequel tu te trouves, c.à.d. le bac à sable)

> 🛈 J'ai décidé arbitrairement de te faire utiliser [codium] dans ce cours, mais sache qu'il existe un paquet d'autres éditeurs de texte, du plus sobre (e.g. `nano`) au plus complet (e.g. `CLion`), souvent appelés [IDE]s quand ils font plus que simple éditeur de texte.

Dans le panneau à gauche, sous "PLAYGROUND", tu devrais trouver une vue du dossier `playground` avec tous les fichiers auto-générés.
Dans `src` (pour "source") tu devrais trouver le fichier `main.rs` qui contient le code source de notre "Hello, world!".

![Hello world source code](./hello-world.excalidraw.svg)

1. Tu la reconnais, c'est la phrase que le programme affiche.
   En programmation, on appelle ça une "chaine de caractères" ou "string" (of characters) en anglais.  
   __Pour l'instant, tu peux retenir que string = texte.__  
   (Un "caractère", en gros, c'est une lettre ('a', 'b', 'c', ...) ou un signe de ponctuation ('!', ';', '?', ...) ;
   Du texte c'est donc une suite de caractères)
1. `println!` c'est une macro (indiqué par le point d'exclamation).  
   __Pour l'instant tu peux considérer que c'est une [fonction](./functions.md).__  
   C'est l'abbréviation de "`print` with a new `l`i`n`e" et elle va s'occuper d'écrire (print) la string qui lui est passée en argument sur ton terminal.
   (Et d'y ajouter un retour à la ligne (new line))
1. Cette syntaxe déclare une nouvelle `f`unctio`n`, qu'on appelle "main", et qui ne prend aucun argument (symbolisé par les parenthèses vides).
   Par convention, cette "main function" (fonction principale) est le point d'entrée du programme.
   C'est son "body" (tout ce qui se trouve dans les accolades (`{}`)) qui va être exécuté au lancement du programme

"Hello, world!" est très utilisé en informatique pour donner un exemple de code simple illustrant un langage, et désigne également par [synecdoque](https://fr.wikipedia.org/wiki/Synecdoque) tout programme ou bout de code qui affiche ce message, voire par extension tout exemple minimaliste d'utilisation d'un outil.

## Exercices

1. Essaye de changer ce qu'affiche le programme.
   Au lieu de "Hello, world!", fais-lui dire ce que tu veux.
   (Il suffit de changer le code source dans codium et de relancer la commande `cargo run`)
1. J'ai parlé de retour à la ligne plus haut ;
   Essaye de changer le code en
   ```rust
   fn main() {
       println!("Hello,");
       println!(" world!");
   }
   ```
   Qu'est-ce que ça affiche?
   Comment lui faire afficher à nouveau "Hello, world!" (sur une-seule ligne) comme au début, mais en gardant ces deux lignes (de code source) séparées?
   (la macro `print!` existe aussi)

[codium]: https://github.com/VSCodium/vscodium#freelibre-open-source-software-binaries-of-vs-code
[IDE]: https://en.wikipedia.org/wiki/Integrated_development_environment
