# Installer un environnement de développement

> 🛈 Il n'est pas nécessaire de comprendre les commandes de ce chapitre. Tu peux te contenter de les copier-coller les unes à la suite des autres sans lire.


> ⚠️ Cybersécurité: executer des commandes trouvées sur internet est un vecteur d'attaque potentiel.
Si tu n'es pas absolument sur·e de comprendre ce que font ces commandes, les executer signifie faire une confiance aveugle à la source d'où tu les as trouvées.

Pour me permettre de maitriser l'environnement de dév que tu vas utiliser, et nous faciliter la tache, je te propose d'installer [Nix](https://nixos.org)

```sh
sh <(curl -L https://nixos.org/nix/install) --no-daemon
```

en activant la fonctionalité `flakes`

```sh
mkdir --parents ~/.config/nix/
echo 'experimental-features = nix-command flakes' >> ~/.config/nix/nix.conf
```

Puis de te créer un dossier `dev` dans lequel tu pourras plus tard regrouper tous tes projets

```sh
mkdir ~/dev
```

Dans ce dossier,

```sh
cd ~/dev
```

télécharge le code source de ce dépot (la page web que tu es en train de lire)

```sh
nix run nixpkgs#git clone https://gitlab.com/ThibaultLemaire/zero-to-rust.git
```

Il y a dedans une petite config pour automatiser ton env de dev. Mais d'abord il nous faut installer [Home Manager]

```sh
nix run home-manager/master -- init
```

On peut maintenant exporter cette conf sur ton système

```sh
sed "s/%USER%/$USER/g" ~/dev/zero-to-rust/home.nix.in > ~/.config/home-manager/home.nix
```

et activer Home Manager

```sh
nix run home-manager/master -- init --switch
```

Tu peux maintenant aller dans le bac à sable,


```sh
cd ~/dev/zero-to-rust/playground
```

lancer fish,

```sh
fish
```

et autoriser direnv (qui va installer tout ce qu'il te faut pour faire du Rust)

```sh
direnv allow
```

Voilà.
Si tout s'est bien passé, tu as maintenant tous les outils nécessaires à la suite du cours.

## Mettre à jour

Si j'ai ajouté ou modifié des choses dans ce cours depuis la première fois où tu as lu ce chapitre, tu peux (dois) re-télécharger ces changements dans ton dossier local

```sh
cd ~/dev/zero-to-rust
git pull
```

(Ça vaut aussi quand j'ajoute une nouvelle leçon ou autre.
D'une manière générale, c'est bien de faire un `git pull` avant de passer aux exercices.)

---

## => ["Hello, world!"](./hello-world.md)


[Home Manager]: https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
