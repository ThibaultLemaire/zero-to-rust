# Zero to Rust

An absolute beginner course to learn the basics of programming, using the [Rust](https://www.rust-lang.org/) language.

Candidate names also included "Learning programming with Rust".

This is an experiment into teaching Rust to a friend who's never programmed before. From this point onwards, everything will be in French.

## Par où commencer?

J'essaye de construire ce cours de manière non-linéaire avec autant que possible des liens entre les chapitres.
Néanmoins, je te conseille de commencer par la théorie.

- Théorie => [C'est quoi le "code source"?](./Lessons/what-is-programming.md)
- Pratique => [Mettre en place l'environnement de développement](./Lessons/setup-dev-env.md)

---

![Skills/Knowledge Tree](skills-slash-knowledge-tree.excalidraw.svg)
